// Uncomment the next line if you want to use bootstrap, don't forget uncomment jQuery defination in webpack.common.js line 93
import 'bootstrap';
import 'jquery-validation';
import Notification from '@vicoders/support/services/Notification';
import Loader from '@vicoders/support/services/Loader';
import axios from 'axios';
import 'slick-carousel/slick/slick.min.js';

$(document).ready(function() {
	var BaseUrl = $('#BaseUrl').attr('data-url');
    $("#login_form_admin").validate({
        rules: {
            email: {
                required: true,
                email: true,
            },

            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Vui lòng không để trống ô này",
                email: "Vui lòng điền đúng định dạng email",
            },

            password: {
                required: "Vui lòng không để trống ô này",
            }
        }
    });

    // $('.slider .list_image').slick({
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     autoplay: true,
    //     pauseOnHover: true,
    //     infinite: true,
    //     autoplaySpeed: 2500,
    // });
});
