<!doctype html>
<html lang="en">

<head>
    <title>Đăng nhập</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dist/app.css') }}">
    <script type="text/javascript" src="{{ asset('assets/dist/app.js') }}"></script>
</head>

<body>
    <div id="BaseUrl" data-url="{{ asset('/') }}"></div>
    <div class="container" id="main">
        <img src="{{asset('assets/images/admin/logo.png')}}" class="img1" />
        <img src="{{asset('assets/images/admin/ten.png')}}" class="img2" />
        <img src="{{asset('assets/images/admin/line.png')}}" class="img3" />
        <img src="{{asset('assets/images/admin/magic.png')}}" class="img4">
        <div class="hotline">
            Hotline: (+84) 835 303 000 <br> Contact@vifonic.vn
        </div>
        <div class="row torong">
            <div class=" col-md-6 col-sm-6 col-6 main1">
                <div class="main11">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-6 main2" style="font-size: 13px">
                <p><b>Điền thông tin đăng nhập<br />Tài khoản được Vifonic cung cấp</b></p>
                <form method="post" id="login_form_admin" action="{{ route('api-login') }}">
                    @csrf
                    <div class="inp">
                        <input type="text" id="email_login" name="email" placeholder="enter email" class="input" />
                    </div>
                    <div class="inp">
                        <input type="password" id="password_login" name="password" placeholder="PassWord" class="input" />
                    </div>
                    <button type="submit" name="submit" value="ĐĂNG NHẬP" class="submit">ĐĂNG NHẬP</button>
                </form>
                <div class="connect"><b>Connect with us</b>
                    <ul>
                        <li><a href="https://www.facebook.com/vifonic.vn"><img src="{{asset('assets/images/admin/face.png')}}" alt=""></a></li>
                        <li><a href="https://www.facebook.com/messages/t/vifonic.vn"><img src="{{asset('assets/images/admin/mess.png')}}" alt=""></a></li>
                        <li><a href="https://zalo.me/0984062393"><img src="{{asset('assets/images/admin/zalo.png')}}" alt=""></a></li>
                    </ul>
                    Privacy Policy
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
</body>

</html>