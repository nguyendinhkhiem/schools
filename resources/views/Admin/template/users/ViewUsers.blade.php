@extends('Admin.layouts.app')

@section('content')

<div id="page-user-view" class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User view</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>User</a>
                </li>
                <li class="active">
                    <strong>User view</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> User info</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Avatar</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="User Email" name="email" value="{{ $user->email }}" id="email_view" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">User Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="User Name" name="username" value="{{ $user->username }}" id="username_view" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">First Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{ $user->first_name }}" id="first_name_view" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Last Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ $user->last_name }}" id="last_name_view" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Phone:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Phone" name="phone_number" value="{{ $user->phone_number }}" id="phone_number_view" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10">
                                            <div class="summernote">
                                                <textarea class="form-control" name="description" id="description_user_{{ $user->id }}" disabled>{{ $user->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Company:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Company" name="company" value="{{ $user->company }}" id="company_view" disabled>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <img src="{{ asset($user->image->url) }}">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="{{ asset($user->image->url) }}">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white" disabled><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="update_user">
                        <div class="btn btn-primary btn-block" id="submit_view_user">
                        	<a href="{{ route('users-admin-update', $user->id) }}">
                            	<span>Update User</span>
                        	</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="pull-right">
            10GB of <strong>250GB</strong> Free.
        </div>
        <div>
            <strong>Copyright</strong> Example Company &copy; 2014-2017
        </div>
    </div>
</div>

@endsection