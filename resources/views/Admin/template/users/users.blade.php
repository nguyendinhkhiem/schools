@extends('Admin.layouts.app')

@section('content')

<div id="page-users" class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User list</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Users</a>
                </li>
                <li class="active">
                    <strong>User list</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        {{-- <div class="ibox-content m-b-sm border-bottom">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="product_name">Product Name</label>
                        <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="price">Price</label>
                        <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="quantity">Quantity</label>
                        <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="1" selected>Enabled</option>
                            <option value="0">Disabled</option>
                        </select>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Email</th>
                                    <th data-hide="phone">First Name</th>
                                    <th data-hide="phone">Last Name</th>
                                    <th data-hide="phone,tablet">Phone</th>
                                    <th data-hide="phone,tablet">Description</th>
                                    <th data-hide="phone,tablet">Công ty</th>
                                    <th data-hide="phone">Status</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($users as $user)
	                                <tr>
	                                    <td>
	                                        {{ $user->email }}
	                                    </td>
	                                    <td>
	                                        {{ $user->first_name }}
	                                    </td>
	                                    <td>
	                                        {{ $user->last_name }}
	                                    </td>
	                                    <td>
	                                        {{ $user->phone_number }}
	                                    </td>
	                                    <td>
	                                        {{ $user->description }}
	                                    </td>
	                                    <td>
	                                        {{ $user->company }}
	                                    </td>
	                                    <td>
	                                        <span class="label label-primary">Active</span>
	                                    </td>
	                                    <td class="text-right">
	                                        <div class="btn-group">
	                                            <a href="{{ route('users-admin-show', $user->id) }}" class="btn-white btn btn-xs">View</a>
	                                            <a href="{{ route('users-admin-update', $user->id) }}" class="btn-white btn btn-xs">Edit</a>
	                                        </div>
	                                    </td>
	                                </tr>
                            	@endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="pagination" id="">
							{!! $users->links() !!}
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection