@extends('Admin.layouts.app')

@section('content')

<div id="page-user-update" class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User create</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>User</a>
                </li>
                <li class="active">
                    <strong>User create</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> User info</a></li>
                    </ul>
                    <form id="form_create_user" action="" method="">
                        <div id="token_user" data-token="{{ $user_auth->getToken() }}"></div>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <fieldset class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Email:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="User Email" name="email" value="" id="email_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">User Name:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="User Name" name="username" value="" id="username_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">First Name:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="First Name" name="first_name" value="" id="first_name_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Last Name:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="" id="last_name_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Phone:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Phone" name="phone_number" value="" id="phone_number_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                            <div class="col-sm-10">
                                                <div class="summernote">
                                                    <textarea class="form-control" name="description" id="description_user_create"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Company:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Company" name="company" value="" id="company_create">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Roles:</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" >
                                                    <option value="schools">School</option>
                                                    <option value="suppliers">Suppliers</option>
                                                    <option value="parents">Parents</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Password:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Password" name="password" value="" id="password_create">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="submit_update">
                            <button type="submit" class="btn btn-primary btn-block" id="submit_update_user">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="pull-right">
            10GB of <strong>250GB</strong> Free.
        </div>
        <div>
            <strong>Copyright</strong> Example Company &copy; 2014-2017
        </div>
    </div>
</div>

@endsection