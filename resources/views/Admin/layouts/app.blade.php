<!DOCTYPE html>
<html>
	@include('Admin.partials.head')
<body>
	@include('Admin.partials.header') 
	<main>
		<div id="wrapper">
			@include('Admin.partials.sidebar')
			@include('Admin.partials.page-wrapper')
		</div>
	</main>
    @include('Admin.partials.footer')
</body>
</html>