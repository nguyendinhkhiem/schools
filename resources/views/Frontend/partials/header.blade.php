<div class="header_schools container">
	<header>
		<div class="item logo_site">
			<div class="logo_text">
				<div class="logo">
					logo
				</div>
				<div class="text_header">
					Trung tâm tài khoản
				</div>
			</div>
		</div>

		<div class="item language_help">
			<div class="help">
				<div class="dropdown">
				    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				        Trợ giúp
				    </button>
				    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				        <a class="dropdown-item" href="#">Action</a>
				        <a class="dropdown-item" href="#">Another action</a>
				        <a class="dropdown-item" href="#">Something else here</a>
				    </div>
				</div>
			</div>

			<div class="language">
				<div class="dropdown">
				    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				        Tiếng Việt
				    </a>
				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="#">English</a>
				    </div>
				</div>
			</div>
		</div>
	</header>
</div>