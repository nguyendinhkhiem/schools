@extends('Frontend.layouts.app')

@section('content')
	<div class="page_login">
		<div class="form_login">
			<div class="text_head_form">
				<p>
					Xin chào! chúng tôi là <span class="name_schools">CLBTruonghoc</span>
				</p>
				<span>
					Chào mừng bạn quay trở lại, xin vui lòng
				</span><br>
				<span>
					đăng nhập vào tài khoản của bạn
				</span>
			</div>
			<div class="social_login">
				<p>Đăng nhập bằng</p>
				<div class="fb item">
					<p><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</p>
				</div>

				<div class="google item">
					<p>
						<i class="fa fa-google" aria-hidden="true"></i> Google
					</p>
				</div>
			</div>
			<div class="form">
				<p>
					Hoặc bằng ID/Email/SĐT
				</p>
				<form id="form_login_frontend">
					<div>
						<input type="text" name="username" id="username">
					</div>
					<div>
						<input type="password" name="password_login" id="password_login">
					</div>
					<div class="remember_forget">
						<div class="remember">
							<div class="form-check">
							    <input class="form-check-input" type="checkbox" name="exampleRadios" id="remember_login" value="option1" checked>
							    <label class="form-check-label" for="remember_login">
							        Ghi nhớ đăng nhập
							    </label>
							</div>
						</div>

						<div class="forget">
							<div>
								<a href="#">Quên mật khẩu</a>
							</div>
						</div>
					</div>
					<div class="login_register">
						<div class="login item">
							<button type="submit" class="btn btn-primary">Đăng nhập</button>
						</div>
						<div class=register_user>
							<div>
								<a href="#">Đăng ký miễn phí</a>
							</div>
						</div>
					</div>
				</form>
				<div class="text_footer_form">
					<span>
						Bằng cách đăng ký, bạn đồng ý với CLB Trường Học về:
					</span>
					<span>
						Các điều khoản & Chính sách bảo mật
					</span>
				</div>
			</div>
		</div>

		<div class="slider">
			<div class="list_image">
				<div class="item">
					<img src="{{ asset('assets/images/1.jpg') }}">
				</div>
			</div>
		</div>
	</div>
@endsection