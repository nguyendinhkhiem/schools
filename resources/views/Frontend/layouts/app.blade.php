<!DOCTYPE html>
<html>
	@include('Frontend.partials.head')
<body>
	<main class="main">
		@include('Frontend.partials.header')
		@yield('content')
	</main>
	@include('Frontend.partials.footer')
</body>
</html>