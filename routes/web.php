<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'View\Admin\UserController@index')->name('login-admin');
Route::post('admin/login', 'Api\Admin\AuthController@authenticate')->name('api-login');

Route::group(['prefix' => 'admin','middleware' => 'admin'], function() {
	Route::get('dashboard', 'View\Admin\DashboardController@index')->name('dashboard-admin');
	Route::get('logout', 'Api\Admin\AuthController@logout')->name('api-logout');

	//users
	Route::get('users/create', 'View\Admin\UserController@store')->name('users-create');
	Route::get('users', 'View\Admin\UserController@list')->name('users-admin');
	Route::get('users/{id}', 'View\Admin\UserController@show')->name('users-admin-show');
	Route::get('users/{id}/update', 'View\Admin\UserController@update')->name('users-admin-update');

});

Route::group(['prefix' => '/'], function() {
	Route::get('login', 'View\Frontend\LoginController@index')->name('page-login');
});