<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
	$api->post('users', 'App\Http\Controllers\Api\Frontend\UserController@store');
	$api->put('users/{id}', 'App\Http\Controllers\Api\Admin\UserController@update');
});
