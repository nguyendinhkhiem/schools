<?php

namespace App\Http\Middleware;
use App\Entities\User;
use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            if($user->isRole('admin')){
                return $next($request);
            }else{
                return redirect('admin/login')->with(['error' => "Tài khoản không có quyền vào trang quản trị !"]);
            }
        }else{
            return redirect('admin/login')->with(['error' => "Bạn chưa đăng nhập !"]);
        }
    }
}
