<?php
namespace App\Http\Controllers\Api\Frontend;

use App\Entities\User;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => ['store']]);
    }

    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data           = $request->all();
        $user           = $this->repository->create($data);
        $user->password = Hash::make($data['password']);
        $user->status   = User::STATUS_ACTIVE;
        $user->save();

        if ($request->has('role')) {
            $user = $this->repository->attachRole($request->get('role'), $user->id);
        }

        $token = JWTAuth::fromUser($user);
        Auth::login($user);

        // Event::fire(new UserRegisteredEvent($user));
        return $this->response->array(compact('token'));
    }
}
