<?php
namespace App\Http\Controllers\Api\Admin;

use App\Entities\User;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new User;

        $query = $query->whereHas('roles', function ($q) use ($request) {
            $q->where('slug', 'schools');
            $q->orWhere('slug', 'suppliers');
            $q->orWhere('slug', 'parents');
        });

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('first_name', 'like', "%{$search}%");
                $q->orWhere('last_name', 'like', "%{$search}%");
                $q->orWhere('email', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 50;
        $users    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->paginator($users, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.users')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_CREATE_USER');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data                 = $request->all();
        $user                 = $this->repository->create($data);
        $user->password       = Hash::make($data['password']);
        $user->email_verified = 1;
        $user->status         = User::STATUS_ACTIVE;
        $user->save();

        if ($request->has('role')) {
            $user = $this->repository->attachRole($request->get('role'), $user->id);
        }

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($this->getAuthenticatedUser()->id);
        if (!$user->can('update.user')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_UPDATE_USER');
        $existsEmail    = $this->repository->existsEmail($id, $request->get('email'));
        $existsUsername = $this->repository->existsUsername($id, $request->get('username'));
        $existsPhone    = $this->repository->existsPhone($id, $request->get('phone_number'));
        if ($existsEmail) {
            throw new ConflictHttpException("Email has exists", null, 1001);
        }

        if ($existsUsername) {
            throw new ConflictHttpException("Username has exists", null, 1001);
        }

        if ($existsPhone) {
            throw new ConflictHttpException("Phone has exists", null, 1001);
        }

        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);

        if ($request->has('status')) {
            $user->status = $request->get('status');
            $user->save();
        }

        return $this->response->item($user, new UserTransformer);
    }
}
