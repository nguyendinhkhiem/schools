<?php

namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'logout']]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $user = $this->repository->findByField('email', $credentials['email'])->first();
        if (!$user) {
            return redirect('admin/login');
        }

        if (!Hash::check($credentials['password'], $user->password)) {
            return redirect('admin/login');
        }

        Auth::attempt($credentials);

        if (!$user->isRole('admin')) {
            return redirect('admin/login');
        }
        return redirect('admin/dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('admin/login');
    }

    public function me()
    {
        try {
            $user = JWTAuth::parseToken()->toUser();
            if (empty($user)) {
                throw new NotFoundException('User');
            }
            if (!$user->isRole('admin')) {
                throw new PermissionDeniedException;
            }
        } catch (Exception $e) {
            return response()->json([], 204);
        }

        Cache::flush();
        return $this->response->item($user, new UserTransformer);
    }
}
