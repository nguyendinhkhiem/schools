<?php
namespace App\Http\Controllers\View\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		return view('Admin.template.index');
	}
}