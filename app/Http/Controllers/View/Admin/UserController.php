<?php
namespace App\Http\Controllers\View\Admin;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect('admin/dashboard');
        }
        return view('Admin.Frontend.login');
    }

    public function store()
    {
        $user_auth = User::find(Auth::user()->id);

        $data = [
            'user_auth' => $user_auth,
        ];

        return view('Admin.template.users.CreateUser', $data);
    }

    public function list() {
        $users = User::latest()
            ->paginate(20);

        $data = [
            'users' => $users,
        ];
        return view('Admin.template.users.users', $data);
    }

    public function show($id)
    {
        $user = User::find($id);

        $user_auth = User::find(Auth::user()->id);

        $data = [
            'user'      => $user,
            'user_auth' => $user_auth,
        ];
        return view('Admin.template.users.ViewUsers', $data);
    }

    public function update($id)
    {
        $user = User::find($id);

        $user_auth = User::find(Auth::user()->id);

        $data = [
            'user'      => $user,
            'user_auth' => $user_auth,
        ];
        return view('Admin.template.users.EditUsers', $data);
    }
}
