<?php
namespace App\Entities;

use App\Entities\Address;
use App\Entities\Image;
use App\Entities\School;
use App\Entities\Student;
use Illuminate\Database\Eloquent\Model;

class Workplace extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'content',
        'description',
    ];

    public function school()
    {
        return $this->hasOne(School::class);
    }

    public function student()
    {
        return $this->hasOne(Student::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }
}
