<?php
namespace App\Entities;

use App\Entities\Caregory;
use App\Entities\Image;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;
    
    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'content',
        'price',
        'description',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function categoris()
    {
        return $this->belongsToMany(Caregory::class);
    }
}
