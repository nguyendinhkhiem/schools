<?php
namespace App\Entities;

use App\Entities\Parent;
use App\Entities\Workplace;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'parent_id',
        'workplace_id',
        'name',
        'slug',
        'image',
        'content',
        'description',
        'status',
    ];

    public function parent()
    {
        return $this->belongsTo(Parent::class);
    }

    public function workplace()
    {
        return $this->belongsTo(Workplace::class);
    }
}
