<?php
namespace App\Entities;

use App\Entities\Post;
use App\Entities\Product;
use Illuminate\Database\Eloquent\Model;

class Caregory extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const CATEGORY_TYPE_POST    = 1;
    const CATEGORY_TYPE_PRODUCT = 0;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'status',
        'type',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
