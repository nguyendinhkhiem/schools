<?php
namespace App\Entities;

use App\Entities\User;
use App\Entities\Workplace;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
	protected $fillable = [
        'user_id',
        'workplace_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function workplace()
    {
        return $this->belongsTo(Workplace::class);
    }
}