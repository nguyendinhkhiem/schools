<?php
namespace App\Entities;

use App\Entities\Address;
use App\Entities\Image;
use App\Entities\Post;
use App\Entities\Product;
use App\Entities\School;
use App\Entities\Student;
use App\Entities\StudentParent;
use App\Entities\Supplier;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use NF\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use NF\Roles\Models\Role;
use NF\Roles\Traits\HasRoleAndPermission;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract, JWTSubject, Transformable
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission, TransformableTrait;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    protected $fillable = [
        'email',
        'password',
        'username',
        'first_name',
        'last_name',
        'phone_number',
        'avatar',
        'description',
        'company',
        'show_hompage',
        'status',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getEmailVerifyToken()
    {
        return Hash::make($this->email);
    }

    public function sendPasswordResetNotification($token)
    {
        // $this->notify(new MailResetPasswordToken($token));
        $this->notify(new AdminAppMailResetPasswordToken($token));
    }

    public function getToken()
    {
        return JWTAuth::fromUser($this);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function school()
    {
        return $this->hasOne(School::class);
    }

    public function supplier()
    {
        return $this->hasOne(Supplier::class);
    }

    public function parent()
    {
        return $this->hasOne(StudentParent::class);
    }

    public function student()
    {
        return $this->hasMany(Student::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function post()
    {
        return $this->hasMany(Post::class);
    }
}
