<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform($model)
    {
        $transform = [
            'id'           => (int) $model->id,
            'email'        => $model->email,
            'username'     => $model->username,
            'first_name'   => $model->first_name,
            'last_name'    => $model->last_name,
            'phone_number' => $model->phone_number,
            'description'  => $model->description,
            'avatar'       => $model->avatar ? $model->avatar : '',
            'status'       => (int) $model->status,
        ];

        $transform['timestamps'] = [
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];

        return $transform;
    }
}
