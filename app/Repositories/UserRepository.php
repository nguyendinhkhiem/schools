<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    public function existsEmail($id, $email);
    public function existsUsername($id, $username);
    public function existsPhone($id, $phone_number);
    public function verifyEmail($user);
    public function attachRole($role, $id);
}
