<?php

namespace App\Repositories;

use App\Entities\User;
use App\Repositories\UserRepository;
use NF\Roles\Models\Role;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function existsEmail($id, $email)
    {
        $user = $this->findWhere([
            'email' => $email,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function existsUsername($id, $username)
    {
        $user = $this->findWhere([
            'username' => $username,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function existsPhone($id, $phone_number)
    {
        $user = $this->findWhere([
            'phone_number' => $phone_number,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function verifyEmail($user)
    {
        $user->email_verified = 1;
        $user->save();
        $this->flushCache();
        return $user;
    }

    public function attachRole($role, $id)
    {
        $user = $this->find($id);
        if (!$user->roles->isEmpty() && !$user->isRole($role)) {
            throw new AccessDeniedHttpException("can not change the role of user", null, 1027);
        }
        $role = Role::where('slug', $role)->first();
        $user->attachRole($role);
        return $user;
    }
}
