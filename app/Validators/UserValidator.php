<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class UserValidator extends AbstractValidator
{

    protected $rules = [
        'ADMIN_CREATE_USER' => [
            'email'        => ['required', 'email'],
            'password'     => ['required', 'min:6', 'max:30'],
            'username'     => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => ['required'],
            'role'         => ['required'],
        ],
        'ADMIN_UPDATE_USER' => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            // 'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
        ],
        'RULE_CREATE'       => [
            'email'        => ['required', 'email'],
            'password'     => ['required', 'min:6', 'max:30'],
            'username'     => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'role'         => ['required'],
        ],
        'RULE_UPDATE'       => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => [],
        ],
        'CHANGE_PASSWORD'   => [
            'email'        => ['required'],
            'old_password' => ['required'],
            'new_password' => ['required'],
        ],
        'UPDATE_PASSWORD'   => [
            'email'    => ['required'],
            'password' => ['required'],
        ],
        'STORE_WISHLIST'    => [
            'product_id' => ['required'],
        ],
        'DELETE_WISHLIST'   => [
            'product_id' => ['required'],
        ],
        'IMAGES'            => [
            'url' => ['required', 'url'],
        ],
        'VERIFY_EMAIL'      => [
            'token' => ['required'],
        ],
        'UPDATE_ROLE'       => [
            'role' => ['required', 'regex:/[customer|artist]/'],
        ],
    ];
}
