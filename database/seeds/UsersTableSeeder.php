<?php
use App\Entities\Image;
use App\Entities\User;
use Illuminate\Database\Seeder;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_US\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));

        $adminRole     = Role::where('slug', 'admin')->first();
        $schoolsRole   = Role::where('slug', 'schools')->first();
        $suppliersRole = Role::where('slug', 'suppliers')->first();
        $parentsRole   = Role::where('slug', 'parents')->first();

        $users = [
            [
                'email'        => 'admin@teg.com',
                'phone_number' => '0123000000',
                'username'     => 'admin',
                'first_name'   => 'schools',
                'last_name'    => 'Admin',
                'company'      => 'Teg',
            ],
            [
                'email'        => 'suppliers@teg.com',
                'phone_number' => '0123000011',
                'username'     => 'suppliers',
                'first_name'   => 'schools',
                'last_name'    => 'teg',
                'company'      => 'Fpt',
            ],
            [
                'email'        => 'schools@teg.com',
                'phone_number' => '0123000022',
                'username'     => 'schools',
                'first_name'   => 'schools',
                'last_name'    => 'teg',
                'company'      => 'VNPT',
            ],
            [
                'email'        => 'student_parents@teg.com',
                'phone_number' => '0123000033',
                'username'     => 'student_parents',
                'first_name'   => 'student_parents',
                'last_name'    => 'teg',
                'company'      => 'Sun*',
            ],
        ];

        foreach ($users as $item) {
            $user               = new User;
            $user->email        = $item['email'];
            $user->phone_number = $item['phone_number'];
            $user->username     = $item['username'];
            $user->first_name   = $item['first_name'];
            $user->last_name    = $item['last_name'];
            $user->company      = $item['company'];
            $user->password     = 'secret';
            $user->status       = User::STATUS_ACTIVE;
            $user->save();

            $avatars = [
                'assets/images/admin/avatar/avatar1.jpg',
                'assets/images/admin/avatar/avatar2.jpg',
                'assets/images/admin/avatar/avatar3.jpg',
                'assets/images/admin/avatar/avatar4.jpg',
            ];

            $image                 = new Image;
            $image->url            = $faker->randomElement($avatars);
            $image->imageable_id   = $user->id;

            $user->image()->save($image);
        }

        $admin     = User::where('username', 'admin')->first()->attachRole($adminRole);
        $schools   = User::where('username', 'schools')->first()->attachRole($schoolsRole);
        $suppliers = User::where('username', 'admin')->first()->attachRole($suppliersRole);
        $parents   = User::where('username', 'admin')->first()->attachRole($parentsRole);
    }
}
