<?php

use App\Entities\Caregory;
use App\Entities\Post;
use App\Entities\Product;
use App\Entities\Student;
use App\Entities\StudentParent;
use App\Entities\Workplace;
use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;
use NF\Roles\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $adminRole     = Role::where('slug', 'admin')->first();
        $schoolsRole   = Role::where('slug', 'schools')->first();
        $suppliersRole = Role::where('slug', 'suppliers')->first();
        $parentsRole   = Role::where('slug', 'parents')->first();

        $permissions             = $this->permissions();
        $admin_permissions     = collect($this->adminPermissions());
        $schools_permissions   = collect($this->schoolsPermissions());
        $suppliers_permissions = collect($this->suppliersPermissions());
        $parents_permissions   = collect($this->parentsPermissions());

        foreach ($permissions as $item) {
            $permission = Permission::updateOrCreate([
                'slug' => $item['slug'],
            ], $item);
        }
        
        $admin_permissions = Permission::whereIn('slug', $admin_permissions->pluck('slug'))->get();
        $adminRole->permissions()->sync($admin_permissions->pluck('id'));

        $schools_permissions = Permission::whereIn('slug', $schools_permissions->pluck('slug'))->get();
        $schoolsRole->permissions()->sync($schools_permissions->pluck('id'));

        $suppliers_permissions = Permission::whereIn('slug', $suppliers_permissions->pluck('slug'))->get();
        $suppliersRole->permissions()->sync($suppliers_permissions->pluck('id'));

        $parents_permissions = Permission::whereIn('slug', $parents_permissions->pluck('slug'))->get();
        $parentsRole->permissions()->sync($parents_permissions->pluck('id'));
    }

    private function permissions()
    {
        return [
            // User
            [
                'name'  => 'View User',
                'slug'  => 'view.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Create User',
                'slug'  => 'create.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Update User',
                'slug'  => 'update.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Delete User',
                'slug'  => 'delete.user',
                'model' => User::class,
            ],

            // Role
            [
                'name'  => 'View Role',
                'slug'  => 'view.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Create Role',
                'slug'  => 'create.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Update Role',
                'slug'  => 'update.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Delete Role',
                'slug'  => 'delete.role',
                'model' => Role::class,
            ],

            // products
            [
                'name'  => 'View Product',
                'slug'  => 'view.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Create Product',
                'slug'  => 'create.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Update Product',
                'slug'  => 'update.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Delete Product',
                'slug'  => 'delete.product',
                'model' => Product::class,
            ],

            //categories
            [
                'name'  => 'View Category',
                'slug'  => 'view.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Create Category',
                'slug'  => 'create.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Update Category',
                'slug'  => 'update.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Delete Category',
                'slug'  => 'delete.category',
                'model' => Caregory::class,
            ],

            //parent
            [
                'name'  => 'View StudentParent',
                'slug'  => 'view.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Create StudentParent',
                'slug'  => 'create.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Update StudentParent',
                'slug'  => 'update.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Delete StudentParent',
                'slug'  => 'delete.student.parent',
                'model' => StudentParent::class,
            ],

            //Post
            [
                'name'  => 'View Post',
                'slug'  => 'view.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Create Post',
                'slug'  => 'create.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Update Post',
                'slug'  => 'update.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Delete Post',
                'slug'  => 'delete.post',
                'model' => Post::class,
            ],

            //Workplace
            [
                'name'  => 'View Workplace',
                'slug'  => 'view.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Create Workplace',
                'slug'  => 'create.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Update Workplace',
                'slug'  => 'update.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Delete Workplace',
                'slug'  => 'delete.workplace',
                'model' => Workplace::class,
            ],

            //Supplier
            [
                'name'  => 'View Supplier',
                'slug'  => 'view.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Create Supplier',
                'slug'  => 'create.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Update Supplier',
                'slug'  => 'update.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Delete Supplier',
                'slug'  => 'delete.supplier',
                'model' => Supplier::class,
            ],

            //Student
            [
                'name'  => 'View Student',
                'slug'  => 'view.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Create Student',
                'slug'  => 'create.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Update Student',
                'slug'  => 'update.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Delete Student',
                'slug'  => 'delete.student',
                'model' => Student::class,
            ],

            //School
            [
                'name'  => 'View School',
                'slug'  => 'view.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Create School',
                'slug'  => 'create.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Update School',
                'slug'  => 'update.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Delete School',
                'slug'  => 'delete.school',
                'model' => School::class,
            ],
        ];
    }

    private function adminPermissions()
    {
        return [
            // User
            [
                'name'  => 'View User',
                'slug'  => 'view.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Create User',
                'slug'  => 'create.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Update User',
                'slug'  => 'update.user',
                'model' => User::class,
            ],
            [
                'name'  => 'Delete User',
                'slug'  => 'delete.user',
                'model' => User::class,
            ],

            // Role
            [
                'name'  => 'View Role',
                'slug'  => 'view.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Create Role',
                'slug'  => 'create.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Update Role',
                'slug'  => 'update.role',
                'model' => Role::class,
            ],
            [
                'name'  => 'Delete Role',
                'slug'  => 'delete.role',
                'model' => Role::class,
            ],

            // products
            [
                'name'  => 'View Product',
                'slug'  => 'view.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Create Product',
                'slug'  => 'create.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Update Product',
                'slug'  => 'update.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Delete Product',
                'slug'  => 'delete.product',
                'model' => Product::class,
            ],

            //categories
            [
                'name'  => 'View Category',
                'slug'  => 'view.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Create Category',
                'slug'  => 'create.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Update Category',
                'slug'  => 'update.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Delete Category',
                'slug'  => 'delete.category',
                'model' => Caregory::class,
            ],

            //parent
            [
                'name'  => 'View StudentParent',
                'slug'  => 'view.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Create StudentParent',
                'slug'  => 'create.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Update StudentParent',
                'slug'  => 'update.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Delete StudentParent',
                'slug'  => 'delete.student.parent',
                'model' => StudentParent::class,
            ],

            //Post
            [
                'name'  => 'View Post',
                'slug'  => 'view.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Create Post',
                'slug'  => 'create.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Update Post',
                'slug'  => 'update.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Delete Post',
                'slug'  => 'delete.post',
                'model' => Post::class,
            ],

            //Workplace
            [
                'name'  => 'View Workplace',
                'slug'  => 'view.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Create Workplace',
                'slug'  => 'create.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Update Workplace',
                'slug'  => 'update.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Delete Workplace',
                'slug'  => 'delete.workplace',
                'model' => Workplace::class,
            ],

            //Supplier
            [
                'name'  => 'View Supplier',
                'slug'  => 'view.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Create Supplier',
                'slug'  => 'create.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Update Supplier',
                'slug'  => 'update.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Delete Supplier',
                'slug'  => 'delete.supplier',
                'model' => Supplier::class,
            ],

            //Student
            [
                'name'  => 'View Student',
                'slug'  => 'view.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Create Student',
                'slug'  => 'create.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Update Student',
                'slug'  => 'update.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Delete Student',
                'slug'  => 'delete.student',
                'model' => Student::class,
            ],

            //School
            [
                'name'  => 'View School',
                'slug'  => 'view.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Create School',
                'slug'  => 'create.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Update School',
                'slug'  => 'update.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Delete School',
                'slug'  => 'delete.school',
                'model' => School::class,
            ],
        ];
    }

    private function schoolsPermissions()
    {
        return [
            // User
            [
                'name'  => 'View User',
                'slug'  => 'view.user',
                'model' => User::class,
            ],
            // [
            //     'name'  => 'Create User',
            //     'slug'  => 'create.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Update User',
            //     'slug'  => 'update.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Delete User',
            //     'slug'  => 'delete.user',
            //     'model' => User::class,
            // ],

            // products
            [
                'name'  => 'View Product',
                'slug'  => 'view.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Create Product',
                'slug'  => 'create.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Update Product',
                'slug'  => 'update.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Delete Product',
                'slug'  => 'delete.product',
                'model' => Product::class,
            ],

            //categories
            [
                'name'  => 'View Category',
                'slug'  => 'view.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Create Category',
                'slug'  => 'create.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Update Category',
                'slug'  => 'update.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Delete Category',
                'slug'  => 'delete.category',
                'model' => Caregory::class,
            ],

            //parent
            [
                'name'  => 'View StudentParent',
                'slug'  => 'view.student.parent',
                'model' => StudentParent::class,
            ],

            //Post
            [
                'name'  => 'View Post',
                'slug'  => 'view.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Create Post',
                'slug'  => 'create.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Update Post',
                'slug'  => 'update.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Delete Post',
                'slug'  => 'delete.post',
                'model' => Post::class,
            ],

            //Workplace
            [
                'name'  => 'View Workplace',
                'slug'  => 'view.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Create Workplace',
                'slug'  => 'create.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Update Workplace',
                'slug'  => 'update.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Delete Workplace',
                'slug'  => 'delete.workplace',
                'model' => Workplace::class,
            ],

            //Supplier
            [
                'name'  => 'View Supplier',
                'slug'  => 'view.supplier',
                'model' => Supplier::class,
            ],

            //Student
            [
                'name'  => 'View Student',
                'slug'  => 'view.student',
                'model' => Student::class,
            ],

            //School
            [
                'name'  => 'View School',
                'slug'  => 'view.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Create School',
                'slug'  => 'create.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Update School',
                'slug'  => 'update.school',
                'model' => School::class,
            ],
            [
                'name'  => 'Delete School',
                'slug'  => 'delete.school',
                'model' => School::class,
            ],
        ];
    }

    private function suppliersPermissions()
    {
        return [
            // User
            [
                'name'  => 'View User',
                'slug'  => 'view.user',
                'model' => User::class,
            ],
            // [
            //     'name'  => 'Create User',
            //     'slug'  => 'create.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Update User',
            //     'slug'  => 'update.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Delete User',
            //     'slug'  => 'delete.user',
            //     'model' => User::class,
            // ],

            // products
            [
                'name'  => 'View Product',
                'slug'  => 'view.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Create Product',
                'slug'  => 'create.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Update Product',
                'slug'  => 'update.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Delete Product',
                'slug'  => 'delete.product',
                'model' => Product::class,
            ],

            //categories
            [
                'name'  => 'View Category',
                'slug'  => 'view.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Create Category',
                'slug'  => 'create.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Update Category',
                'slug'  => 'update.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Delete Category',
                'slug'  => 'delete.category',
                'model' => Caregory::class,
            ],

            //parent
            [
                'name'  => 'View StudentParent',
                'slug'  => 'view.student.parent',
                'model' => StudentParent::class,
            ],

            //Post
            [
                'name'  => 'View Post',
                'slug'  => 'view.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Create Post',
                'slug'  => 'create.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Update Post',
                'slug'  => 'update.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Delete Post',
                'slug'  => 'delete.post',
                'model' => Post::class,
            ],

            //Workplace
            [
                'name'  => 'View Workplace',
                'slug'  => 'view.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Create Workplace',
                'slug'  => 'create.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Update Workplace',
                'slug'  => 'update.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Delete Workplace',
                'slug'  => 'delete.workplace',
                'model' => Workplace::class,
            ],

            //Supplier
            [
                'name'  => 'View Supplier',
                'slug'  => 'view.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Create Supplier',
                'slug'  => 'create.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Update Supplier',
                'slug'  => 'update.supplier',
                'model' => Supplier::class,
            ],
            [
                'name'  => 'Delete Supplier',
                'slug'  => 'delete.supplier',
                'model' => Supplier::class,
            ],

            //Student
            [
                'name'  => 'View Student',
                'slug'  => 'view.student',
                'model' => Student::class,
            ],

            //School
            [
                'name'  => 'View School',
                'slug'  => 'view.school',
                'model' => School::class,
            ],
        ];
    }

    private function parentsPermissions()
    {
        return [
            // User
            [
                'name'  => 'View User',
                'slug'  => 'view.user',
                'model' => User::class,
            ],
            // [
            //     'name'  => 'Create User',
            //     'slug'  => 'create.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Update User',
            //     'slug'  => 'update.user',
            //     'model' => User::class,
            // ],
            // [
            //     'name'  => 'Delete User',
            //     'slug'  => 'delete.user',
            //     'model' => User::class,
            // ],

            // products
            [
                'name'  => 'View Product',
                'slug'  => 'view.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Create Product',
                'slug'  => 'create.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Update Product',
                'slug'  => 'update.product',
                'model' => Product::class,
            ],
            [
                'name'  => 'Delete Product',
                'slug'  => 'delete.product',
                'model' => Product::class,
            ],

            //categories
            [
                'name'  => 'View Category',
                'slug'  => 'view.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Create Category',
                'slug'  => 'create.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Update Category',
                'slug'  => 'update.category',
                'model' => Caregory::class,
            ],
            [
                'name'  => 'Delete Category',
                'slug'  => 'delete.category',
                'model' => Caregory::class,
            ],

            //parent
            [
                'name'  => 'View StudentParent',
                'slug'  => 'view.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Create StudentParent',
                'slug'  => 'create.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Update StudentParent',
                'slug'  => 'update.student.parent',
                'model' => StudentParent::class,
            ],
            [
                'name'  => 'Delete StudentParent',
                'slug'  => 'delete.student.parent',
                'model' => StudentParent::class,
            ],

            //Post
            [
                'name'  => 'View Post',
                'slug'  => 'view.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Create Post',
                'slug'  => 'create.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Update Post',
                'slug'  => 'update.post',
                'model' => Post::class,
            ],
            [
                'name'  => 'Delete Post',
                'slug'  => 'delete.post',
                'model' => Post::class,
            ],

            //Workplace
            [
                'name'  => 'View Workplace',
                'slug'  => 'view.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Create Workplace',
                'slug'  => 'create.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Update Workplace',
                'slug'  => 'update.workplace',
                'model' => Workplace::class,
            ],
            [
                'name'  => 'Delete Workplace',
                'slug'  => 'delete.workplace',
                'model' => Workplace::class,
            ],

            //Supplier
            [
                'name'  => 'View Supplier',
                'slug'  => 'view.supplier',
                'model' => Supplier::class,
            ],

            //Student
            [
                'name'  => 'View Student',
                'slug'  => 'view.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Create Student',
                'slug'  => 'create.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Update Student',
                'slug'  => 'update.student',
                'model' => Student::class,
            ],
            [
                'name'  => 'Delete Student',
                'slug'  => 'delete.student',
                'model' => Student::class,
            ],

            //School
            [
                'name'  => 'View School',
                'slug'  => 'view.school',
                'model' => School::class,
            ],
        ];
    }
}
