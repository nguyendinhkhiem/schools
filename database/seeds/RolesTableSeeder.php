<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'  => 'Admin',
                'slug'  => 'admin',
                'level' => 5,
            ],
            [
                'name'  => 'Schools',
                'slug'  => 'schools',
                'level' => 4,
            ],
            [
                'name'  => 'Suppliers',
                'slug'  => 'suppliers',
                'level' => 4,
            ],
            [
                'name'  => 'Parents',
                'slug'  => 'parents',
                'level' => 3,
            ]
        ];

        foreach ($data as $item) {
            $role        = new Role;
            $role->name  = $item['name'];
            $role->slug  = $item['slug'];
            $role->level = $item['level'];
            $role->save();
        }
    }
}
