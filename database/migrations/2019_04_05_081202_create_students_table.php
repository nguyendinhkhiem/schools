<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('student_parent_id')->unsigned()->index();
            $table->foreign('student_parent_id')->references('id')->on('student_parents')->onDelete('cascade');
            $table->unsignedBigInteger('workplace_id')->unsigned()->index();
            $table->foreign('workplace_id')->references('id')->on('workplaces')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->string('content');
            $table->text('description');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
